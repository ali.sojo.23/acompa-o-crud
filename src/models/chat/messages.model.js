const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const countryShema = new schema(
	{
		Chat: {
			type: schema.Types.ObjectId,
			ref: "Conversations",
		},
		User: {
			type: schema.Types.ObjectId,
			ref: "Users",
		},
		Message: {
			type: String,
			required: true,
		},
		Status: {
			type: Number,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Messages", countryShema);
