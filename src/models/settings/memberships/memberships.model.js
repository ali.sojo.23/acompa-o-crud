const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const membershipsSchema = new schema({
	title: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		required: true,
	},
	skills: {
		type: Array,
		required: true,
	},
	avatar: {
		type: String,
		required: true,
	},
	cost: {
		type: Number,
		required: true,
	},
	rate: {
		type: schema.Types.ObjectId,
		ref: "rates",
		required: true,
	},
	days: {
		type: Number,
		required: true,
	},
});

module.exports = mongoose.model("memberships", membershipsSchema);
