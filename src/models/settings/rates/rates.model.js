const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const rateSchema = new schema({
	country: {
		type: schema.Types.ObjectId,
		ref: "Country",
		required: true,
	},
	rate: {
		type: Number,
		required: true,
	},
	tax: {
		type: Number,
		required: true,
	},
	code: {
		type: String,
		required: true,
	},
});

module.exports = mongoose.model("rates", rateSchema);
