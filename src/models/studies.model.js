const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const studiesShema = new schema(
	{
		Institute: {
			type: String,
			required: true,
		},
		Grade: {
			type: String,
		},
		Subject: {
			type: String,
			required: true,
		},
		Start: {
			type: String,
			required: true,
		},
		End: {
			type: String,
		},
		User: {
			type: schema.Types.ObjectId,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Study", studiesShema);
