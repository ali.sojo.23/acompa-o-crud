const mongoose = require("mongoose");
const schema = mongoose.Schema;
mongoose.set("useCreateIndex", true);

const orderSchema = new schema(
	{
		order_id: {
			type: Number,
			unique: true,
			required: true,
		},
		user: {
			type: schema.Types.ObjectId,
			ref: "Users",
			required: true,
		},
		services: {
			type: schema.Types.ObjectId,
			ref: "services",
		},
		membership: {
			type: schema.Types.ObjectId,
			ref: "memberships",
		},
		membership_family: {
			type: Boolean,
			default: false,
		},
		family_members: {
			type: Number,
			default: 1,
		},
		soft_delete: {
			type: Boolean,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("order", orderSchema);
