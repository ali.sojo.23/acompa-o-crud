const express = require("express");
const router = express.Router();
const auth = require("../controllers/auth/auth.controller");

router.post("/register", auth.createUser);
router.post("/login", auth.loginUser);
router.post("/forgot/email", auth.sendEmailToResetPassword);
router.post("/verify/resend/:id", auth.resendEmail);
router.get("/verify/:tkn", auth.verifyEmail);
router.put("/status", auth.changeStatus);

module.exports = router;
