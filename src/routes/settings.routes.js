const express = require("express");
const router = express.Router();
const rates = require("../controllers/settings/rates/rates.controller");
const memberships = require("../controllers/settings/memberships/memberships.controller");
//  Rutas para las tarifas
router.get("/rates/:id", rates.GET);
router.post("/rates/", rates.POST);
router.post("/rates/:id", rates.UPDATE);
router.get("/rates", rates.GETALL);
// Rutas para las membresías
router.get("/memberships", memberships.GET);
router.post("/memberships", memberships.POST);
router.get("/memberships/:id", memberships.SHOW);
router.post("/memberships/:id", memberships.UPDATE);
router.delete("/memberships/:id", memberships.DELETE);
module.exports = router;
