const express = require("express");
const order = require("../controllers/accounting/order/order.controller");
const router = express.Router();

router.post("/orders", order.POST);
router.get("/orders/:id", order.SHOW);
router.delete("/orders/:id", order.DELETE);

module.exports = router;
