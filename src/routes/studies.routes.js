const express = require("express");
const router = express.Router();
const Studies = require("../controllers/studies/study.controller");

router.post("/", Studies.POST);
router.get("/", Studies.GET);
router.get("/:id", Studies.findById);

module.exports = router;
