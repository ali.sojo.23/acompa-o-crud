const order = require("../../../models/orders/orders.model");

exports.POST = async (req, res) => {
	let { usuario, membresia } = req.body;
	let orders;
	let last = await order
		.findOne()
		.sort({ order_id: -1 })
		.catch((err) => {
			return res.status("409").send(err);
		});
	console.log(last);

	if (last) {
		orders = {
			membership: membresia,
			user: usuario,
			order_id: last.order_id + 1,
		};
	} else {
		orders = {
			membership: membresia,
			user: usuario,
			order_id: 1,
		};
	}

	orders = await new order(orders).save().catch((err) => {
		return res.status("409").send(err);
	});

	if (orders) res.status("200").send(orders);
};

exports.SHOW = async (req, res) => {
	let { id } = req.params;
	let orders = await order
		.findOne({ _id: id })
		.populate({
			path: "membership",
			populate: {
				path: "rate",
				populate: {
					path: "country",
				},
			},
		})
		.populate({
			path: "user",
			populate: {
				path: "Auth",
				select: "email",
			},
			populate: {
				path: "informacion",
				populate: {
					path: "Province",
					populate: {
						path: "Country",
						select: "Country",
					},
				},
			},
		})
		.catch((err) => {
			return res.status("409").send(err);
		});

	if (orders) res.status("200").send(orders);
};

exports.DELETE = async (req, res) => {
	let { id } = req.params;
	let orders = await order
		.findByIdAndUpdate(id, { soft_delete: true })
		.catch((err) => {
			res.status("409").send(err);
		});
	res.status("200").send({ orders, message: "orders delete" });
};
