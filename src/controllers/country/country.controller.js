const Country = require("../../models/country.model");

//
//
//GET ALL
//
//
exports.GET = async (req, res) => {
	country = await Country.find();
	if (country) {
		res.send({ country });
	} else {
		res.send({
			message: "Error cannot find nothing",
		});
	}
};
//
//
// CREATR NEW USER
//
//
exports.POST = async (req, res) => {
	const data = {
		Country: req.body.country,
	};
	console.log(req.body);

	country = await new Country(data).save().catch((err) => {
		return res
			.status("409")
			.send(
				"Error: bad request, verify your data input and try again.",
				err
			);
	});

	res.send({ country });
};
//
//
// GET COUNTRY OBTAINED USING AUTH ID
//
//
exports.GetById = async (req, res) => {
	const { id } = req.params;
	country = await Country.findById(id);
	if (country) {
		res.send({ country });
	} else {
		res.send({ message: "Country not found" });
	}
};
