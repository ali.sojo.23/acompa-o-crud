const Auth = require("./auth.dao");
const Users = require("../../models/users.model");
const Info = require("../../models/info.model");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { SECRET_KEY, expiresIn } = require("../../config/tokenize");
const nm = require("nodemailer");
const mail = require("../../config/mail.config");

exports.createUser = (req, res, next) => {
	let { FirstName, LastName, email, password, confirm_password } = req.body;
	if (!FirstName || !LastName || !email || !password || !confirm_password)
		return res.status(409).send("incomplete path");
	if (password != confirm_password)
		return res.status(409).send("invalid path");
	const newAuth = {
		email: req.body.email,
		password: bcrypt.hashSync(req.body.password),
	};

	Auth.create(newAuth, async (err, auth) => {
		if (err && err.code === 11000)
			return res.status(409).send("Email already exists");
		if (err) return res.status(500).send("Server error");
		const accessToken = jwt.sign({ id: auth.id }, SECRET_KEY, {
			expiresIn: expiresIn,
		});
		const info = await new Info().save().catch((err) => console.log(err));

		const newUser = {
			Nombre: req.body.FirstName,
			Apellido: req.body.LastName,
			Auth: auth.id,
			Role: req.body.Role,
			informacion: info.id,
		};

		const user = await new Users(newUser).save().catch((err) => {
			return res.status(409).send(err);
		});
		const User = {
			user,
			accessToken: accessToken,
			expiresIn: Date.now() + expiresIn,
		};
		if (user) {
			const send = nm.createTransport(mail.transporter);
			const url = process.env.PUBLIC_URL + "auth/verify/" + accessToken;
			const mailOptions = {
				from: "Uvitale Web App <" + mail.transporter.auth.user + ">",
				to: newAuth.email,
				subject: "Verify Email",
				html:
					"<b>Hola nuevo mensaje</b> " +
					"<a href=" +
					url +
					"> Verificar Email </a>",
			};
			await send.sendMail(mailOptions).catch((err) => console.log(err));

			res.send({ User });
		}
		// res.send({ User });
	});
};

exports.loginUser = async (req, res, next) => {
	const userData = {
		email: req.body.email,
		password: req.body.password,
	};
	await Auth.findOne({ email: userData.email }, async (err, auth) => {
		if (err) return res.status(500).send("Server error!");

		if (!auth) {
			// email does not exist
			res.status(409).send({ message: "Something is wrong" });
		} else {
			const resultPassword = bcrypt.compareSync(
				userData.password,
				auth.password
			);
			if (resultPassword) {
				const accessToken = jwt.sign({ id: auth.id }, SECRET_KEY, {
					expiresIn: expiresIn,
				});
				auth = await Auth.findByIdAndUpdate(auth._id, { status: 1 });

				const user = await Users.findOne({ Auth: auth.id }).populate(
					"informacion"
				);
				const User = {
					user,
					status: auth.status,
					accessToken: accessToken,
					expiresIn: Date.now() + expiresIn,
				};
				console.log(User);
				res.send({ User });
			} else {
				// password wrong
				res.status(409).send({ message: "Something is wrong" });
			}
		}
	});
};

exports.resendEmail = async (req, res) => {
	const { id } = req.params;
	const user = await Auth.findById(id);
	if (user) {
		const Token = jwt.sign({ id: user._id }, SECRET_KEY, {
			expiresIn: "1d",
		});
		const send = nm.createTransport(mail.transporter);
		const url = process.env.PUBLIC_URL + "auth/verify/" + Token;
		const mailOptions = {
			from: "Uvitale Web App <" + mail.transporter.auth.user + ">",
			to: user.email,
			subject: "Verify Email",
			html:
				"<b>Hola nuevo mensaje</b> " +
				"<a href=" +
				url +
				"> Verificar Email </a>",
		};
		await send.sendMail(mailOptions);
		res.send({ message: 200 });
	}
};

exports.verifyEmail = async (req, res) => {
	var tkn = req.params.tkn;
	tkn = jwt.verify(tkn, SECRET_KEY);
	const user = await Auth.findByIdAndUpdate(tkn.id, {
		email_verified_at: Date.now(),
	});
	res.send(user);
};

exports.sendEmailToResetPassword = async (req, res) => {
	const email = req.body.email;
	const auth = await Auth.findOne({ email: email });
	if (!auth) {
		res.send({ code: 404, error: "Email not found" });
	} else {
		const Token = jwt.sign({ auth: auth.email }, SECRET_KEY, {
			expiresIn: "1d",
		});
		const send = nm.createTransport(mail.transporter);
		const url = "http://localhost:8100/reset/password/" + Token;
		const mailOptions = {
			from: "Uvitale Web App <" + mail.transporter.auth.user + ">",
			to: auth.email,
			subject: "Reset password <" + email + "> - Uvitale Web App",
			html:
				"<b>Hola nuevo mensaje</b> " +
				"<a href=" +
				url +
				"> Reset Password </a>",
		};
		await send.sendMail(mailOptions);
		res.status(200).send({ message: "Email sended" });
	}
};

exports.changeStatus = async (req, res) => {
	let { status, id } = req.body;
	const auth = await Auth.findByIdAndUpdate(id, { status: status });
	res.status(200).send({ message: "your actual status is changed" });
};
