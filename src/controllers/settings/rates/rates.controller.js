const Rate = require("../../../models/settings/rates/rates.model");
exports.GET = async (req, res) => {
	var rate = await Rate.findOne({ country: req.params.id }).catch((err) => {
		return res.status("409").send(err);
	});
	if (!rate) {
		res.status("204").send({ message: "rates not found" });
	} else {
		res.status("200").send(rate);
	}
};
exports.POST = async (req, res) => {
	var rate = {
		country: req.body.country,
		rate: req.body.rate,
		code: req.body.code,
		tax: req.body.tax,
	};
	rate = await new Rate(rate).save().catch((err) => {
		res.status("409").send(
			"message: Error saving info please try again",
			err
		);
	});

	res.status("200").send(rate);
};

exports.UPDATE = async (req, res) => {
	const { id } = req.params;
	var rate = await Rate.findByIdAndUpdate(id, req.body).catch((err) => {
		return res.status("409").send({ err });
	});
	res.status("200").send(rate);
};

exports.GETALL = async (req, res) => {
	let rate = await Rate.find()
		.populate("country")
		.catch((err) => {
			res.status("409").send(err);
		});
	if (rate) {
		res.status("200").send(rate);
	}
};
