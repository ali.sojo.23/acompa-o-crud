const Memberships = require("../../../models/settings/memberships/memberships.model");
exports.GET = async (req, res) => {
	var data = await Memberships.find()
		.populate({
			path: "rate",
			populate: {
				path: "country",
			},
		})
		.catch((err) => {
			return res.status("409").send(err);
		});
	if (!data) {
		res.status("204").send({ message: "rates not found" });
	} else {
		res.status("200").send(data);
	}
};
exports.SHOW = async (req, res) => {
	let { id } = req.params;
	var data = await Memberships.findById(id).catch((err) => {
		return res.status("409").send(err);
	});
	if (!data) {
		res.status("204").send({ message: "rates not found" });
	} else {
		res.status("200").send(data);
	}
};
exports.POST = async (req, res) => {
	var data = {
		skills: req.body.skill,
		title: req.body.title,
		description: req.body.description,
		avatar: req.body.avatar,
		cost: req.body.cost,
		rate: req.body.country,
		days: req.body.days,
	};
	data = await new Memberships(data).save().catch((err) => {
		res.status("409").send(err);
	});

	res.status("200").send(data);
};

exports.UPDATE = async (req, res) => {
	const { id } = req.params;
	var memberships = await Memberships.findByIdAndUpdate(id, req.body).catch(
		(err) => {
			return res.status("409").send({ err });
		}
	);
	res.status("200").send(memberships);
};

exports.DELETE = async (req, res) => {
	let { id } = req.params;
	await Memberships.deleteOne({ _id: id }).catch((err) => {
		return res.status("409").send(err);
	});
	res.status("200").send({ message: "Membresía Eliminada correctamente" });
};
