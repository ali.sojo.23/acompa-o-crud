const env = require("../../.env");
let db = "";
if (env.PRODUCTION) {
	db = "mongodb://45.79.47.113:27017/acompao";
} else {
	db = "mongodb://localhost:27017/xn--acompao";
}
module.exports = {
	PORT: process.env.PORT || env.HOST_PORT,
	DB: db,
};
