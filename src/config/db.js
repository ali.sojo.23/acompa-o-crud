const mongoose = require("mongoose");
const env = require("../../.env");
const dbURL = require("./properties").DB;

module.exports = () => {
	mongoose.set("useFindAndModify", false);
	if (env.PRODUCTION) {
		mongoose
			.connect(dbURL, {
				auth: { authSource: env.DB_AUTHENTICABLE_DATA_BASE },
				user: env.DB_USER,
				pass: env.DB_PASSWORD,
				useUnifiedTopology: true,
				useNewUrlParser: true,
			})
			.then(() => console.log(`Mongo connected on ${dbURL}`))
			.catch((err) => console.log(`Connection has error ${err}`));
	} else {
		mongoose
			.connect(dbURL, {
				useUnifiedTopology: true,
				useNewUrlParser: true,
			})
			.then(() => console.log(`Mongo connected on ${dbURL}`))
			.catch((err) => console.log(`Connection has error ${err}`));
	}
	process.on("SIGINT", () => {
		mongoose.connection.close(() => {
			console.log(`Mongo is disconnected`);
			process.exit(0);
		});
	});
};
