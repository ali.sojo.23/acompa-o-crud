const env = require("../../.env");

var SECRET_KEY = "";

if (!SECRET_KEY) {
	SECRET_KEY = env.SECRET_KEY;
}

module.exports = {
	SECRET_KEY: SECRET_KEY,
	expiresIn: 2629800000,
};
