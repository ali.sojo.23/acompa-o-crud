"use strict";

const express = require("express");
const path = require("path");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const { SECRET_KEY } = require("../.env");
const methodOverride = require("method-override");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
//Config
//
const propierties = require("./config/properties");
const DB = require("./config/db");
const env = require("../.env");
process.env.PUBLIC_URL = env.HOST;
// init DB

DB();

// Middleware
const bodyParser = require("body-parser");
const bodyParserJSON = bodyParser.json({ limit: "50mb", extended: true });
const bodyParserURLEncoded = bodyParser.urlencoded({
	limit: "10mb,",
	extended: true,
});

app.use(bodyParserJSON);
app.use(bodyParserURLEncoded);

app.use(cors());

app.use(methodOverride("X-HTTP-Method")); //          Microsoft
app.use(methodOverride("X-HTTP-Method-Override")); // Google/GData
app.use(methodOverride("X-Method-Override")); //      IBM
const tokenize = express.Router();
tokenize.use((req, res, next) => {
	const token = req.headers["access-token"];

	if (token) {
		jwt.verify(token, SECRET_KEY, (err, decoded) => {
			if (err) {
				return res.json({ mensaje: "Token inválida" });
			} else {
				req.decoded = decoded;
				next();
			}
		});
	} else {
		res.status(401).send("Error <b>401 Unauthorized</b>");
	}
});

// Routes
const frontRoutes = require("./routes/frontend.routes");
app.use("/", frontRoutes);

const authRoutes = require("./routes/auth.routes");
app.use("/auth", authRoutes);

const userRoutes = require("./routes/user.routes");
app.use("/user", tokenize, userRoutes);

const studiesRoutes = require("./routes/studies.routes");
app.use("/education", tokenize, studiesRoutes);

const countryRoutes = require("./routes/country.routes");
app.use("/country", tokenize, countryRoutes);

const provincesRoutes = require("./routes/province.routes");
app.use("/province", tokenize, provincesRoutes);

const chatRoutes = require("./routes/chat.routes");
app.use("/chat", chatRoutes);

const settingsRoutes = require("./routes/settings.routes");
app.use("/settings", tokenize, settingsRoutes);

const ordersRoutes = require("./routes/accounting.routes");
app.use("/accounting", tokenize, ordersRoutes);

// Static Files

app.use(
	"/public/uploads",
	express.static(path.join(__dirname, "public/uploads"))
);

// Socket io
var connection = 0;
io.on("connection", (socket) => {
	socket.on("disconnect", function () {
		io.emit("users-changed", { user: socket.username, event: "left" });
	});

	socket.on("set-name", (name) => {
		socket.username = name;
		io.emit("users-changed", { user: name, event: "joined" });
	});

	socket.on("send-message", (message) => {
		io.emit("message", {
			msg: message.text,
			user: socket.username,
			createdAt: new Date(),
		});
	});
});
// Launch Server and port
http.listen(propierties.PORT, () =>
	console.log(`Server runing on port ${propierties.PORT}`)
);
